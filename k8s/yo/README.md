
```shell
eval $(minikube docker-env -u)
docker build -t yo:v1 .
docker login
docker tag yo:v1 k33g/yo:v1
docker push k33g/yo:v1 # docker push $DOCKER_ID_USER/yo:v1

```

```shell
kubectl create -f deployment.yml
kubectl create -f service.yml

minikube service yo
```

## Updates (it probably exists a best way)

```shell
docker build --no-cache -t yo:v3 .
docker tag yo:v3 k33g/yo:v3
docker push k33g/yo:v3
kubectl delete deployment yo
kubectl create -f deployment.yml
minikube service yo

```

```shell
minikube addons enable ingress
kubectl create -f yo-ingress.yml
kubectl create -f hello-ingress.yml
kubectl create -f hey-ingress.yml
```