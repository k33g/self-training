

## First: install

## Initialize minikube VM

```shell
minikube start --memory 6144 
eval $(minikube docker-env) # undo: eval $(minikube docker-env -u) TODO retry GitLab install with this
# kubectl config use-context minikube ???
```

## cmd

```shell
kubectl cluster-info
minikube dashboard
```

## hello world

```shell
kubectl run hello-minikube --image=gcr.io/google_containers/echoserver:1.4 --port=8080
#deployment "hello-minikube" created
kubectl expose deployment hello-minikube --type=NodePort
#service "hello-minikube" exposed
kubectl delete deployment hello-minikube
kubectl delete service hello-minikube
#deployment "hello-minikube" deleted

# again
kubectl run hello-minikube --image=gcr.io/google_containers/echoserver:1.4 --port=8080

# Get pods, but what does it means???
kubectl get pods

# get some informations
curl $(minikube service hello-minikube --url)
```

a pod = the smallest and simplest k8s object
a pod = a set of running containers on your cluster


## Docker

```shell
docker build -t hello-node:v1 .
# somewhere
kubectl run hello-node --image=hello-node:v1 --port=8080
kubectl get deployments
kubectl get pods
kubectl get events
kubectl config view


docker build -t hey-node:v1 .
kubectl run hey-node --image=hey-node:v1 --port=8080
```

By default, the Pod is only accessible by its internal IP address within the Kubernetes cluster. To make the hello-node Container accessible from outside the Kubernetes virtual network, you have to expose the Pod as a Kubernetes Service.

From your development machine, you can expose the Pod to the public internet using the kubectl expose command:

```shell
kubectl expose deployment hello-node --type=LoadBalancer

kubectl expose deployment hey-node --type=LoadBalancer

kubectl get services
#hello-node 10.108.192.13:8080   
```

The --type=LoadBalancer flag indicates that you want to expose your Service outside of the cluster

```shell
minikube service hello-node  
minikube service hey-node 

# gas mask
# 192.168.99.102  hello.local http://hello.local:32678/
# 192.168.99.102 hey.local http://hey.local:32614/

```

### Update images

- change code source

```shell
# before docker build -t hello-node:v1 .
docker build -t hello-node:v2 .
# update the image of the deployment
kubectl set image deployment/hello-node hello-node=hello-node:v2
```

## resources

- docker --version 
- https://darkowlzz.github.io/post/minikube-config/
- https://kubernetes.io/docs/tutorials/stateless-application/hello-minikube/


## Docker part

https://hub.docker.com/
https://store.docker.com

