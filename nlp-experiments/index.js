const natural = require('natural')


let stemmer = natural.PorterStemmer
let stemmerFR = natural.PorterStemmerFr

let my_string = "Hi, 👋 I'm Philippe, I'm working at GitLab as Technical account manager"
let my_fr_string = "Salut, je suis Philippe, je travaille chez GitLab comme Technical account manager"


let tokenizer = new natural.WordTokenizer()
let otherTokenizer = new natural.WordPunctTokenizer()
//let tokenizerAgain = new natural.TreebankWordTokenizer()

console.log(tokenizer.tokenize(my_string))
console.log(otherTokenizer.tokenize(my_string))

console.log(stemmer.tokenizeAndStem(my_string))
//[ 'hi', 'philipp', 'work', 'gitlab', 'technic', 'account', 'manag' ]
console.log(stemmerFR.tokenizeAndStem(my_fr_string))


/*
https://jolicode.com/blog/natural-language-processing-avec-des-petits-morceaux-de-nodejs-dedans
https://egghead.io/lessons/node-js-break-up-language-strings-into-parts-using-natural

*/