const natural = require('natural')
let NGrams = natural.NGrams
let stemmer = natural.PorterStemmer
//console.log(stemmer.tokenizeAndStem(my_string))

let my_string = "hi @Bob Hi, I'm Philippe, give me ,  pricing of clever I'm working at GitLab as Technical account manager"

console.log(stemmer.tokenizeAndStem(my_string))

//console.log(NGrams.bigrams(my_string).find(item => item.join('-') == [ 'hi', 'Bob' ].join('-')))
//console.log(NGrams.trigrams(my_string).find(item => item.join('-') == [ 'give', 'me', 'pricing' ].join('-')))
//console.log(NGrams.ngrams(my_string, 5).find(item => item.join('-') == [ 'give', 'me', 'pricing', 'of', 'clever' ].join('-')))

console.log(
  NGrams.bigrams(stemmer.tokenizeAndStem(my_string).join(' '))
    .find(item => item.join('-') == [ 'hi', 'bob' ].join('-'))
)
console.log(
  NGrams.trigrams(stemmer.tokenizeAndStem(my_string).join(' '))
    .find(item => item.join('-') == [ 'give', 'price', 'clever' ].join('-'))
)

/*
https://jolicode.com/blog/natural-language-processing-avec-des-petits-morceaux-de-nodejs-dedans
https://egghead.io/lessons/node-js-break-up-language-strings-into-parts-using-natural

*/